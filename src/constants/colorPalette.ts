const colorPalette = {
  DARK_CERULEAN_BLUE: '#f5f8fa',
  BIG_STONE_BLUE: '#353f45',
  BIG_STONE_BLUE_20: '#33353f45',
  DODGER_BLUE: '#0575ff',
  NIGHT_RIDER_GREY: '#333333',
  SOLITUDE_BLUE: '#F5F8FA',
};

export default colorPalette;
