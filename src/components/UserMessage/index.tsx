import { Box } from '@material-ui/core';
import React from 'react';
import MessageText from '../MessageText';
import MessageTimestamp from '../MessageTimestamp';

interface IUserMessage {
  message: string;
  timestamp: string;
}

const UserMessage: React.FC<IUserMessage> = ({ message, timestamp }) => {
  return (
    <Box display="flex" justifyContent="flex-end" mb="20px">
      <Box>
        <MessageTimestamp type="user">{timestamp}</MessageTimestamp>
        <MessageText type="user">{message}</MessageText>
      </Box>
    </Box>
  );
};

export default UserMessage;
