import { combineReducers } from 'redux';
import chatReducer, { IChatState } from './chat/reducers';
import uiSlice, { IUIState } from './ui/slice';

export interface IRootReducer {
  ui: IUIState;
  chat: IChatState;
}

const rootReducer = combineReducers<IRootReducer>({
  ui: uiSlice.reducer,
  chat: chatReducer,
});

export default rootReducer;
