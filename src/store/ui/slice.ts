// import { Draft } from 'immer';
import { Action, createSlice } from 'redux-starter-kit';

export interface IUIState {
  isChatWindowOpen: boolean;
  isChatStarted: boolean;
  isMenuOpen: boolean;
}

/* export interface IUICaseReducers {
  showChatWindow: Action<'ui/showChatWindow'>;
  minimizeChatWindow: Action<'ui/minimizeChatWindow'>;
}
export interface IUICaseReducers {
  showChatWindow: (
    state: Draft<IUIState>,
    action: Action<'ui/showChatWindow'>
  ) => IUIState | void;
  minimizeChatWindow: (
    state: Draft<IUIState>,
    action: Action<'ui/minimizeChatWindow'>
  ) => IUIState | void;
} */

const uiSlice = createSlice<IUIState, any>({
  slice: 'ui',
  initialState: { isChatWindowOpen: true, isChatStarted: true, isMenuOpen: false },
  reducers: {
    showChatWindow: (state: IUIState, action: Action<'ui/showChatWindow'>) => {
      state.isChatWindowOpen = true;
    },
    minimizeChatWindow: (
      state: IUIState,
      action: Action<'ui/minimizeChatWindow'>
    ) => {
      state.isChatWindowOpen = false;
    },
    startChat: (state: IUIState, action: Action<'ui/startChat'>) => {
      state.isChatStarted = true;
    },
    stopChat: (state: IUIState, action: Action<'ui/stopChat'>) => {
      state.isChatStarted = false;
    },
    showMenu: (state: IUIState, action: Action<'ui/showMenu'>) => {
      state.isMenuOpen = true;
    },
    closeMenu: (state: IUIState, action: Action<'ui/closeMenu'>) => {
      state.isMenuOpen = false;
    },
  },
});

export default uiSlice;
