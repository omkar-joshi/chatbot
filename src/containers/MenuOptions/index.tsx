import { Divider, ListItemIcon, makeStyles } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {
  CancelOutlined,
  ChatBubbleOutline,
  ExitToApp,
  Refresh,
} from '@material-ui/icons';
import React from 'react';
import fonts from '../../constants/fonts';

const useStyles = makeStyles({
  root: {
    position: 'absolute',
  },
  backdrop: {
    position: 'absolute',
  },
  container: {
    alignItems: 'flex-end',
  },
  paperFullWidth: {
    width: 'calc(100% - 30px)',
  },
  paper: {
    margin: 15,
  },
  listItemText: {
    fontFamily: fonts.RiposteRegular,
    fontSize: 16,
  },
});

interface IMenuOptions {
  onClose: () => void;
  open: boolean;
}

const MenuOptions: React.FC<IMenuOptions> = props => {
  const { onClose, open } = props;
  const classes = useStyles();

  function handleClose() {
    onClose();
  }

  function handleListItemClick() {
    onClose();
  }

  return (
    <Dialog
      disablePortal
      onClose={handleClose}
      open={open}
      fullWidth
      className={classes.root}
      classes={{
        root: classes.root,
        container: classes.container,
        paperFullWidth: classes.paperFullWidth,
        paper: classes.paper,
      }}
      BackdropProps={{
        classes: { root: classes.backdrop },
      }}
      style={{ position: 'absolute' }}
    >
      <List>
        <ListItem button onClick={() => handleListItemClick()}>
          <ListItemIcon>
            <ChatBubbleOutline />
          </ListItemIcon>
          <ListItemText
            primary="Download Chat"
            className={classes.listItemText}
          />
        </ListItem>
        <Divider />
        <ListItem button onClick={() => handleListItemClick()}>
          <ListItemIcon>
            <ExitToApp />
          </ListItemIcon>
          <ListItemText
            primary="Exit Conversation"
            className={classes.listItemText}
          />
        </ListItem>
        <Divider />
        <ListItem button onClick={() => handleListItemClick()}>
          <ListItemIcon>
            <Refresh />
          </ListItemIcon>
          <ListItemText primary="Restart" className={classes.listItemText} />
        </ListItem>
        <Divider />
        <ListItem button onClick={() => handleListItemClick()}>
          <ListItemIcon>
            <CancelOutlined />
          </ListItemIcon>
          <ListItemText primary="Cancel" className={classes.listItemText} />
        </ListItem>
      </List>
    </Dialog>
  );
};

export default MenuOptions;
