import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React from 'react';

const useStyles = makeStyles(theme => ({
  poweredBy: {
    fontFamily: 'RiposteLight',
    fontSize: 12,
  },
}));

const PoweredBy: React.FC = ({children}) => {
  const classes = useStyles();

  return (
    <Typography align="right" className={classes.poweredBy}>
      {children}
    </Typography>
  );
};

export default PoweredBy;
