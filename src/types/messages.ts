export interface IMessage<
  F /* extends 'user' | 'bot' */,
  M /* extends IUserMessageM | IBotMessageM */
> {
  from: F;
  message: M;
}
export interface IUserMessage extends IMessage<'user', IUserMessageM> {}
export interface IBotMessage extends IMessage<'bot', IBotMessageM> {}

export interface IMessageM {
  text: string;
  user: string;
  channel: 'socket';
}
export interface IUserMessageM extends IMessageM {
  type: 'message' | 'message_received';
}

export interface IListButton {
  title: string;
  payload: string;
}

export interface IBotMessageListTypeData {
  label: string;
  description: string;
  payload: string;
}
export interface IBotMessageCarouselTypeData {
  label: string;
  description: string;
  buttons: IListButton[];
  image_type: 'link';
  image: string;
}

type IFieldType =
  | 'text'
  | 'email'
  | 'phone'
  | 'number'
  | 'date'
  | 'time'
  | 'select'
  // | 'checkbox'
  | 'checkbox_boolean'
  | 'checkbox_list'
  | 'radio';

export interface IOption {
  value: string;
  label: string;
}

export interface IField {
  name: string;
  placeholder: string;
  label: string;
  required: boolean;
  type: IFieldType;
  options: IOption[];
}

export interface IBotMessageFormTypeData {
  primaryButtonLabel: string;
  secondaryButtonLabel: string;
  fields: IField[];
}

type IBotMessageMType = 'list' | 'message' | 'typing' | 'carousel' | 'form';

export interface IQuickReply {
  title: string;
  payload: string;
}

export interface IBotMessageM extends IMessageM {
  type: IBotMessageMType;
  continue_typing: boolean;
  sent_timestamp: number;
  timestamp: number;
  to: string;
  delay: number;
  data:
    | IBotMessageCarouselTypeData[]
    | IBotMessageListTypeData[]
    | IBotMessageFormTypeData;
  quick_replies: IQuickReply[];
  quick_replies_multiselect: IQuickReply[];
}
