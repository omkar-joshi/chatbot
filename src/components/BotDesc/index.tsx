import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React from 'react';

const useStyles = makeStyles(theme => ({
  botDesc: {
    fontFamily: 'RiposteLight',
    fontSize: 18,
  },
}));

const BotDesc: React.FC = ({ children }) => {
  const classes = useStyles();

  return (
    <Typography align="center" className={classes.botDesc}>
      {children}
    </Typography>
  );
};

export default BotDesc;
