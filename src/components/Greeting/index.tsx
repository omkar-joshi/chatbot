import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React from 'react';

const useStyles = makeStyles(theme => ({
  greeting: {
    fontFamily: 'RiposteLight',
    fontSize: 30,
    color: 'white',
  },
}));

const Greeting: React.FC = () => {
  const classes = useStyles();

  return (
    <Typography className={classes.greeting}>Hi, we're Velotio</Typography>
  );
};

export default Greeting;
