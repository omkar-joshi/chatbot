import { Box, Link, makeStyles } from '@material-ui/core';
import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import AvatarVelo from '../../components/AvatarVelo';
import BotDesc from '../../components/BotDesc';
import BotName from '../../components/BotName';
import Greeting from '../../components/Greeting';
import StartConvButton from '../../components/StartConvButton';
import SubGreeting from '../../components/SubGreeting';
import ToSAgreement from '../../components/ToSAgreement';
import uiSlice from '../../store/ui/slice';

const useStyles = makeStyles(theme => ({
  avatar: {
    marginTop: -100,
  },
}));

const LandingPage: React.FC = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const startChat = useCallback(
    () => dispatch(uiSlice.actions.startChat(undefined)),
    [dispatch]
  );

  return (
    <React.Fragment>
      <Box height={216} bgcolor="primary.main" p="20px">
        <Greeting />
        <SubGreeting />
      </Box>
      <Box
        p="20px"
        display="flex"
        flexDirection="column"
        justifyContent="space-between"
        flexGrow={1}
        height="calc(100% - 216px)"
      >
        <Box display="flex" flexDirection="column" alignItems="center">
          <AvatarVelo size={160} className={classes.avatar} />
          <Box>
            <BotName>Velo</BotName>
            <BotDesc>Typically responds in seconds, everytime!</BotDesc>
          </Box>
        </Box>
        <Box>
          <Box display="flex" justifyContent="center" mt="40px">
            <StartConvButton onClick={startChat}>
              &nbsp;Start Conversation
            </StartConvButton>
          </Box>
          <ToSAgreement>
            By clicking on ‘Start Conversation’, you agree to our{' '}
            <Link>Terms and Conditions</Link>
          </ToSAgreement>
        </Box>
      </Box>
    </React.Fragment>
  );
};

export default LandingPage;
