import { Avatar, makeStyles } from '@material-ui/core';
import clsx from 'clsx';
import React from 'react';
import Velo from '../../assets/Captain_Marvel_EW_Textless_Cover.png';

const useStyles = makeStyles(theme => ({
  avatar: {
    width: (size: number) => size,
    height: (size: number) => size,
  },
}));

interface IAvatarVelo {
  size: number;
  className?: string;
}

const AvatarVelo: React.FC<IAvatarVelo> = ({ size, className }) => {
  const classes = useStyles(size);

  return (
    <Avatar
      alt="Velotia"
      src={Velo}
      className={className ? clsx(classes.avatar, className) : classes.avatar}
    />
  );
};

export default AvatarVelo;
