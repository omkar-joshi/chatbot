import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import uiSlice from '../../store/ui/slice';
import AppWindow from '../AppWindow';
import LaunchButton from '../LaunchButton';

const App: React.FC = () => {
  const isChatWindowOpen = useSelector(uiSlice.selectors.getUi)
    .isChatWindowOpen;
  const dispatch = useDispatch();
  const showChatWindow = useCallback(
    () => dispatch(uiSlice.actions.showChatWindow(undefined)),
    [dispatch]
  );
  const minimizeChatWindow = useCallback(
    () => dispatch(uiSlice.actions.minimizeChatWindow(undefined)),
    [dispatch]
  );

  return isChatWindowOpen ? (
    <AppWindow onHide={minimizeChatWindow} />
  ) : (
    <LaunchButton onClick={showChatWindow} />
  );
};

export default App;
