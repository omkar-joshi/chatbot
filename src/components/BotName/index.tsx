import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React from 'react';

const useStyles = makeStyles(theme => ({
  botName: {
    fontFamily: 'RiposteBold',
    fontSize: 32,
  },
}));

const BotName: React.FC = ({ children }) => {
  const classes = useStyles();

  return (
    <Typography align="center" className={classes.botName}>
      {children}
    </Typography>
  );
};

export default BotName;
