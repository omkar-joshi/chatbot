import { SvgIcon } from '@material-ui/core';
import { SvgIconProps } from '@material-ui/core/SvgIcon';
import React from 'react';
import { ReactComponent as VelotioLogo } from '../assets/VelotioLogo.svg';

const Logo: React.FC<SvgIconProps> = props => {
  return (
    <SvgIcon {...props}>
      <VelotioLogo />
    </SvgIcon>
  );
};

export default Logo;
