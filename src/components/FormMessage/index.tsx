// Picker
import DateFnsUtils from '@date-io/date-fns';
import {
  // Box,
  Box,
  Button,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  MenuItem,
  Paper,
  RadioGroup,
  Theme,
} from '@material-ui/core';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { makeStyles } from '@material-ui/styles';
import { Checkbox, Radio, TextField } from 'final-form-material-ui';
import React from 'react';
import { Field, Form } from 'react-final-form';
import colorPalette from '../../constants/colorPalette';
import { IBotMessageFormTypeData, IField, IOption } from '../../types/messages';
import DatePickerWrapper from '../DatePickerWrapper';
import TimePickerWrapper from '../TimePickerWrapper';

const validate = (values: any) => {
  const errors: any = {};
  if (!values.firstName) {
    errors.firstName = 'Required';
  }
  if (!values.lastName) {
    errors.lastName = 'Required';
  }
  if (!values.email) {
    errors.email = 'Required';
  }

  return errors;
};

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    border: `1px solid ${colorPalette.BIG_STONE_BLUE_20}`,
    padding: `15px 25px`,
    borderRadius: 6,
  },
  formButton: {
    width: 166,
  },
}));

interface IFormMessage {
  config: IBotMessageFormTypeData;
  onSubmit: (filledForm: any) => void;
}

const FormMessage: React.FC<IFormMessage> = ({ config, onSubmit }) => {
  const classes = useStyles();

  return (
    <Form
      onSubmit={onSubmit}
      initialValues={{ employed: true, stooge: 'larry' }}
      validate={validate}
      render={({ handleSubmit, form, submitting, pristine, values }) => (
        <form onSubmit={handleSubmit} noValidate>
          <Paper elevation={0} className={classes.root}>
            {config.fields.map((field: IField) => {
              switch (field.type) {
                case 'text':
                  return (
                    <Field
                      fullWidth
                      required={field.required}
                      name={field.name}
                      component={TextField}
                      type="text"
                      label={field.label}
                      // variant="filled"
                    />
                  );
                case 'email':
                  return (
                    <Field
                      fullWidth
                      required={field.required}
                      name={field.name}
                      component={TextField}
                      type="email"
                      label={field.label}
                      // variant="filled"
                    />
                  );
                case 'checkbox_boolean':
                  return (
                    <FormControlLabel
                      label={field.label}
                      control={
                        <Field
                          name={field.name}
                          component={Checkbox}
                          type="checkbox"
                        />
                      }
                    />
                  );
                case 'radio':
                  return (
                    <FormControl component="fieldset">
                      <FormLabel component="legend">{field.label}</FormLabel>
                      <RadioGroup row={field.options.length <= 2}>
                        {field.options.map((option: IOption) => (
                          <FormControlLabel
                            key={option.value}
                            label={option.label}
                            control={
                              <Field
                                name={field.name}
                                component={Radio}
                                type="radio"
                                value={option.value}
                              />
                            }
                          />
                        ))}
                      </RadioGroup>
                    </FormControl>
                  );

                case 'checkbox_list':
                  return (
                    <FormControl component="fieldset">
                      <FormLabel component="legend">{field.label}</FormLabel>
                      <FormGroup>
                        {field.options.map((option: IOption) => (
                          <FormControlLabel
                            key={option.value}
                            label={option.label}
                            control={
                              <Field
                                name={field.label}
                                component={Checkbox}
                                type="checkbox"
                                value={option.value}
                              />
                            }
                          />
                        ))}
                      </FormGroup>
                    </FormControl>
                  );

                case 'select':
                  return (
                    <Field
                      fullWidth
                      name={field.name}
                      select
                      component={TextField}
                      label={field.label}
                      formControlProps={{ fullWidth: true }}
                      // variant="filled"
                    >
                      {field.options.map((option: IOption) => (
                        <MenuItem key={option.value} value={option.value}>
                          {option.label}
                        </MenuItem>
                      ))}
                    </Field>
                  );
                case 'date':
                  return (
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <Field
                        name={field.name}
                        component={DatePickerWrapper}
                        fullWidth
                        margin="normal"
                        label={field.label}
                        // variant="filled"
                      />
                    </MuiPickersUtilsProvider>
                  );
                case 'time':
                  return (
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <Field
                        name={field.name}
                        component={TimePickerWrapper}
                        fullWidth
                        margin="normal"
                        label={field.label}
                        // variant="filled"
                      />
                    </MuiPickersUtilsProvider>
                  );
              }
            })}

            <Box display="flex" justifyContent="space-around">
              <Button
                type="button"
                variant="outlined"
                onClick={form.reset}
                disabled={submitting || pristine}
                className={classes.formButton}
              >
                {config.secondaryButtonLabel}
              </Button>

              <Button
                variant="contained"
                color="primary"
                type="submit"
                disabled={submitting}
                className={classes.formButton}
              >
                {config.primaryButtonLabel}
              </Button>
            </Box>
          </Paper>
        </form>
      )}
    />
  );
};

export default FormMessage;
