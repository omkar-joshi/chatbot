import { Box, Fab, Theme } from '@material-ui/core';
import { Send as SendIcon } from '@material-ui/icons';
import { makeStyles } from '@material-ui/styles';
import React, { useEffect, useRef, useState } from 'react';
import TextareaAutosize from 'react-textarea-autosize';
import BotMessage from '../../components/BotMessage';
import FormMessage from '../../components/FormMessage';
import ImageCarousel from '../../components/ImageCarousel';
import QuickReplies from '../../components/QuickReplies';
import SelectList from '../../components/SelectList';
import UserMessage from '../../components/UserMessage';
import colorPalette from '../../constants/colorPalette';
import fonts from '../../constants/fonts';
import { IBotMessageFormTypeData, IBotMessageM } from '../../types/messages';
import useChat from './useChat';

/*tslint:disable*/
const useStyles = makeStyles<Theme, {}, string>(theme => ({
  root: {},
  msgsWrapper: {
    overflowX: 'hidden',
    overflowY: 'auto'
  },
  textarea: {
    flexGrow: 1,
    border: 'none',
    backgroundColor: 'transparent',
    color: colorPalette.BIG_STONE_BLUE,
    fontFamily: fonts.RiposteLight,
    fontSize: 16,
    margin: 15,
    outline: 'none',
    maxHeight: 48,
    resize: 'none'
  },
  textControl: {
    minHeight: 48,
    backgroundColor: colorPalette.DARK_CERULEAN_BLUE,
    paddingRight: 15
  },
  sendButton: {
    minHeight: 32,
    height: 32,
    width: 32
  },
  sendIcon: {
    fontSize: 16
  },
  botTimestamp: {
    fontFamily: fonts.RiposteLight,
    fontSize: 13,
    color: colorPalette.NIGHT_RIDER_GREY,
    marginBottom: 4
  },
  userTimestamp: {
    textAlign: 'right',
    fontFamily: fonts.RiposteLight,
    fontSize: 13,
    color: colorPalette.NIGHT_RIDER_GREY,
    marginBottom: 4
  },
  botMsg: {
    padding: 14,
    backgroundColor: colorPalette.SOLITUDE_BLUE,
    fontFamily: fonts.RiposteLight,
    fontSize: 16,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8,
    color: colorPalette.BIG_STONE_BLUE
  },
  userMsg: {
    padding: 14,
    backgroundColor: colorPalette.DODGER_BLUE,
    fontFamily: fonts.RiposteLight,
    fontSize: 16,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    borderBottomLeftRadius: 8,
    color: theme.palette.primary.contrastText
  },
  botAvatar: {
    marginRight: 22
  }
}));

const ChatScreen: React.FC = () => {
  const messagesEnd = useRef<HTMLDivElement>(null);
  const { chat, connectRequest, disconnectRequest, sendRequest } = useChat();
  console.log(JSON.stringify(chat));
  const classes = useStyles();
  const [text, setText] = useState('');
  const handleEnterKeyPress = (
    event: React.KeyboardEvent<HTMLTextAreaElement>
  ) => {
    if (event.key === 'Enter') {
      event.preventDefault();
      if (text) {
        sendRequest(text);
        setText('');
      }
    }
  };

  const handleSendClick = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    if (text) {
      sendRequest(text);
      setText('');
    }
  };

  const scrollToBottom = () => {
    if (messagesEnd && messagesEnd.current) {
      messagesEnd.current.scrollIntoView({ behavior: 'smooth' });
    }
  };

  useEffect(() => {
    connectRequest();

    return disconnectRequest;
  }, []);

  useEffect(() => {
    scrollToBottom();
  });

  /*tslint:disable*/
  return (
    <Box display="flex" flexDirection="column" height="100%">
      <Box p="15px" height="calc(100% - 48px)" className={classes.msgsWrapper}>
        {chat.chat.map((message, index) => {
          switch (message.from) {
            case 'bot':
              switch (message.message.type) {
                case 'message':
                  return (
                    <BotMessage
                      key={index}
                      timestamp="11:58 AM"
                      message={message.message.text}
                    />
                  );
                case 'list':
                  return (
                    <SelectList
                      key={index}
                      data={message.message}
                      onSelect={selection => {
                        console.log(selection);
                        sendRequest(selection.payload);
                      }}
                      isDisabled={chat.chat.length !== index + 1}
                    />
                  );
                case 'carousel':
                  return (
                    <ImageCarousel
                      key={index}
                      data={message.message}
                      onSelect={selection => {
                        console.log(selection);
                        sendRequest(selection.payload);
                      }}
                      disabled={chat.chat.length !== index + 1}
                    />
                  );
                case 'form':
                  return (
                    <FormMessage
                      config={message.message.data as IBotMessageFormTypeData}
                      onSubmit={(filledForm: any) => {
                        console.log(filledForm);
                      }}
                    />
                  );
                default:
                  return undefined;
              }
              break;
            case 'user':
              switch (message.message.type) {
                case 'message':
                  return (
                    <UserMessage
                      key={index}
                      timestamp="11:58 AM"
                      message={message.message.text}
                    />
                  );
                default:
                  return undefined;
              }
          }
        })}
        {/* <BotMessage timestamp="11:58 AM" message="Hello! So, where you from?" /> */}
        {/* <QuickReplies /> */}
        {/* <UserMessage timestamp="11:58 AM" message="North America" /> */}
        {/* <BotMessage timestamp="11:58 AM" message="Hello! So, where you from?" /> */}
        {/* <ImageCarousal /> */}
        {/* <UserMessage timestamp="11:58 AM" message="North America" /> */}
        {/* <SelectList /> */}
        {chat &&
        chat.chat &&
        chat.chat.length &&
        chat.chat[chat.chat.length - 1].message.type === 'typing' ? (
          <BotMessage timestamp="11:58 AM" message="typing" />
        ) : (
          undefined
        )}
        {chat &&
        chat.chat &&
        chat.chat.length &&
        (chat.chat[chat.chat.length - 1].message as IBotMessageM)
          .quick_replies /* ||
        (chat.chat[chat.chat.length - 1].message as IBotMessageM)
          .quick_replies_multiselect  */ ? (
          <QuickReplies
            quickReplies={
              (chat.chat[chat.chat.length - 1].message as IBotMessageM)
                .quick_replies
            }
            onSelect={selection => {
              console.log(selection);
              sendRequest(selection);
            }}
            multiSelect={
              false /* 
              (chat.chat[chat.chat.length - 1].message as IBotMessageM)
                .quick_replies_multiselect
                ? true
                : false
             */
            }
          />
        ) : (
          undefined
        )}
        <div style={{ float: 'left', clear: 'both' }} ref={messagesEnd} />
      </Box>
      <Box
        minHeight="48px"
        display="flex"
        className={classes.textControl}
        alignItems="center">
        <TextareaAutosize
          value={text}
          onChange={e => setText(e.currentTarget.value)}
          onKeyPress={handleEnterKeyPress}
          placeholder="Type your message here..."
          minRows={1}
          maxRows={3}
          className={classes.textarea}
          autoFocus
        />
        <Fab
          size="small"
          color="primary"
          aria-label="send"
          className={classes.sendButton}
          onClick={handleSendClick}>
          <SendIcon className={classes.sendIcon} />
        </Fab>
      </Box>
    </Box>
  );
};

export default ChatScreen;
