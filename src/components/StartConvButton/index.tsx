import { Fab, Typography } from '@material-ui/core';
import { Send } from '@material-ui/icons';
import { makeStyles } from '@material-ui/styles';
import React from 'react';

const useStyles = makeStyles(theme => ({
  startButtonText: {
    fontFamily: 'RiposteRegular',
    fontSize: 15,
    textTransform: 'capitalize',
  },
  startButton: {
    paddingRight: 32,
    paddingLeft: 32,
    boxShadow: 'none',
  },
  sendIcon: {
    fontSize: 16,
  },
}));

interface IStartConvButton {
  onClick: () => void;
}

const StartConvButton: React.FC<IStartConvButton> = ({ onClick, children }) => {
  const classes = useStyles();

  return (
    <Fab
      variant="extended"
      color="primary"
      aria-label="Start"
      onClick={onClick}
      className={classes.startButton}
    >
      <Send className={classes.sendIcon} />
      <Typography
        align="center"
        className={classes.startButtonText}
        component="span"
      >
        {children}
      </Typography>
    </Fab>
  );
};

export default StartConvButton;
