import { Box, useMediaQuery, useTheme } from '@material-ui/core';
import React from 'react';

const AppWindow: React.FC = ({ children }) => {
  const theme = useTheme();
  const isMobDevice = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <Box
      width={isMobDevice ? '100vw' : 450}
      height={isMobDevice ? '100vh' : 736}
      maxHeight={isMobDevice ? '100vh' : '95vh'}
      borderColor="grey.300"
      border={1}
      borderRadius="borderRadius"
      position={isMobDevice ? undefined : 'fixed'}
      right={isMobDevice ? undefined : 20}
      bottom={isMobDevice ? undefined : 20}
      overflow="hidden"
    >
      {children}
    </Box>
  );
};

export default AppWindow;
