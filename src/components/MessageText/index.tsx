import { Theme, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React from 'react';
import colorPalette from '../../constants/colorPalette';
import fonts from '../../constants/fonts';

const useStyles = makeStyles<Theme, { type: string }>(theme => ({
  message: {
    padding: 14,
    backgroundColor: props =>
      props.type === 'bot'
        ? colorPalette.SOLITUDE_BLUE
        : colorPalette.DODGER_BLUE,
    fontFamily: fonts.RiposteLight,
    fontSize: 16,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    borderBottomRightRadius: props => (props.type === 'bot' ? 8 : 'initial'),
    borderBottomLeftRadius: props => (props.type === 'user' ? 8 : 'initial'),
    color: props =>
      props.type === 'bot'
        ? colorPalette.BIG_STONE_BLUE
        : theme.palette.primary.contrastText,
  },
}));

interface IMessageText {
  type: 'bot' | 'user';
  children: string;
}
const MessageText: React.FC<IMessageText> = ({ type, children }) => {
  const classes = useStyles({ type });

  return <Typography className={classes.message}>{children}</Typography>;
};

export default MessageText;
