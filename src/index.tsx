import { CssBaseline } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import App from './containers/App';
import * as serviceWorker from './serviceWorker';
import configureStore from './store';
import theme from './theme';

import './assets/fonts/fonts.css';
import './index.css';

const store = configureStore();

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Provider store={store}>
      <CssBaseline />
      <App />
    </Provider>
  </ThemeProvider>,
  document.getElementById('root')
);

// const chatbot = {
//   render: () => {
//     const appRoot = document.createElement('div');
//     document.body.appendChild(appRoot);
//     ReactDOM.render(
//       <ThemeProvider theme={theme}>
//         <Provider store={store}>
//           <CssBaseline />
//           <App />
//         </Provider>
//       </ThemeProvider>,
//       appRoot
//     );
//   },
// };
// (window as any).chatbot = chatbot;

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
