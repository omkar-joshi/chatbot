import { createMuiTheme } from '@material-ui/core/styles';
import colorPalette from '../constants/colorPalette';

/* import RiposteExtraBoldWoff from '../assets/fonts/riposte-extrabold-webfont.woff';
import RiposteExtraBoldWoff2 from '../assets/fonts/riposte-extrabold-webfont.woff2';

import RiposteHeavyWoff from '../assets/fonts/riposte-heavy-webfont.woff';
import RiposteHeavyWoff2 from '../assets/fonts/riposte-heavy-webfont.woff2';

import RiposteLightWoff from '../assets/fonts/riposte-light-webfont.woff';
import RiposteLightWoff2 from '../assets/fonts/riposte-light-webfont.woff2';

import RiposteMediumWoff from '../assets/fonts/riposte-medium-webfont.woff';
import RiposteMediumWoff2 from '../assets/fonts/riposte-medium-webfont.woff2';

import RiposteRegularWoff from '../assets/fonts/riposte-regular-webfont.woff';
import RiposteRegularWoff2 from '../assets/fonts/riposte-regular-webfont.woff2';

import RiposteSemiBoldWoff from '../assets/fonts/riposte-semibold-webfont.woff';
import RiposteSemiBoldWoff2 from '../assets/fonts/riposte-semibold-webfont.woff2';

import RiposteThinWoff from '../assets/fonts/riposte-thin-webfont.woff';
import RiposteThinWoff2 from '../assets/fonts/riposte-thin-webfont.woff2';

import RiposteBlackWoff from '../assets/fonts/riposte-black-webfont.woff';
import RiposteBlackWoff2 from '../assets/fonts/riposte-black-webfont.woff2';

import RiposteBoldWoff from '../assets/fonts/riposte-bold-webfont.woff';
import RiposteBoldWoff2 from '../assets/fonts/riposte-bold-webfont.woff2';

import RiposteExtraLightWoff from '../assets/fonts/riposte-extralight-webfont.woff';
import RiposteExtraLightWoff2 from '../assets/fonts/riposte-extralight-webfont.woff2';

const unicodeRange =
  'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF';

const RiposteExtraBold = {
  fontFamily: 'RiposteExtraBold',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  fontWeight: 'normal',
  src: `url(${RiposteExtraBoldWoff2}) format('woff2'),url(${RiposteExtraBoldWoff}) format('woff')`,
  unicodeRange,
};

const RiposteHeavy = {
  fontFamily: 'RiposteHeavy',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  fontWeight: 'normal',
  src: `url(${RiposteHeavyWoff2}) format('woff2'),url(${RiposteHeavyWoff}) format('woff')`,
  unicodeRange,
};

const RiposteLight = {
  fontFamily: 'RiposteLight',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  fontWeight: 'normal',
  src: `url(${RiposteLightWoff2}) format('woff2'),url(${RiposteLightWoff}) format('woff')`,
  unicodeRange,
};

const RiposteMedium = {
  fontFamily: 'RiposteMedium',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  fontWeight: 'normal',
  src: `url(${RiposteMediumWoff2}) format('woff2'),url(${RiposteMediumWoff}) format('woff')`,
  unicodeRange,
};

const RiposteRegular = {
  fontFamily: 'RiposteRegular',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  fontWeight: 'normal',
  src: `url(${RiposteRegularWoff2}) format('woff2'),url(${RiposteRegularWoff}) format('woff')`,
  unicodeRange,
};

const RiposteSemiBold = {
  fontFamily: 'RiposteSemiBold',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  fontWeight: 'normal',
  src: `url(${RiposteSemiBoldWoff2}) format('woff2'),url(${RiposteSemiBoldWoff}) format('woff')`,
  unicodeRange,
};

const RiposteThin = {
  fontFamily: 'RiposteThin',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  fontWeight: 'normal',
  src: `url(${RiposteThinWoff2}) format('woff2'),url(${RiposteThinWoff}) format('woff')`,
  unicodeRange,
};

const RiposteBlack = {
  fontFamily: 'RiposteBlack',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  fontWeight: 'normal',
  src: `url(${RiposteBlackWoff2}) format('woff2'),url(${RiposteBlackWoff}) format('woff')`,
  unicodeRange,
};

const RiposteBold = {
  fontFamily: 'RiposteBold',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  fontWeight: 'normal',
  src: `url(${RiposteBoldWoff2}) format('woff2'),url(${RiposteBoldWoff}) format('woff')`,
  unicodeRange,
};

const RiposteExtraLight = {
  fontFamily: 'RiposteExtraLight',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  fontWeight: 'normal',
  src: `url(${RiposteExtraLightWoff2}) format('woff2'),url(${RiposteExtraLightWoff}) format('woff')`,
  unicodeRange,
}; */

export default createMuiTheme({
  /* overrides: {
    MuiCssBaseline: {
      '@global': {
        '@font-face': [
          RiposteExtraBold,
          RiposteHeavy,
          RiposteLight,
          RiposteMedium,
          RiposteRegular,
          RiposteSemiBold,
          RiposteThin,
          RiposteBlack,
          RiposteBold,
          RiposteExtraLight,
        ],
      },
    },
  }, */
  palette: {
    background: {
      default: '#fff',
    },
    primary: { main: colorPalette.DODGER_BLUE },
  },
});
