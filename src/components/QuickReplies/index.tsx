import { Box, Chip, makeStyles, Theme } from '@material-ui/core';
import React, { useState } from 'react';
import fonts from '../../constants/fonts';
import { IQuickReply } from '../../types/messages';
import QuickReply from '../QuickReply';

const useStyles = makeStyles((theme: Theme) => ({
  confirmButton: {
    // marginBottom: 15,
    // marginRight: 15,
    height: 40,
    borderRadius: 20,
    fontFamily: fonts.RiposteLight,
    fontSize: 15,
    marginLeft: 68
  }
}));
interface IQuickReplies {
  quickReplies: IQuickReply[];
  onSelect: (item: string) => void;
  multiSelect: boolean;
}

const QuickReplies: React.FC<IQuickReplies> = ({
  quickReplies,
  onSelect,
  multiSelect
}) => {
  const classes = useStyles();
  const [multiSelectedQuickReplies, multiSelectQuickreply] = useState<
    Set<string>
  >(new Set());

  return (
    <>
      <Box display="flex" ml="68px" flexWrap="wrap">
        {quickReplies.map((quickReply, index) => (
          <QuickReply
            key={index}
            quickReplyText={quickReply.title}
            onClick={
              multiSelect
                ? multiSelectedQuickReplies.has(quickReply.payload)
                  ? () => {
                      const newSelected = new Set(multiSelectedQuickReplies);
                      newSelected.delete(quickReply.payload);
                      multiSelectQuickreply(newSelected);
                    }
                  : () => {
                      multiSelectQuickreply(
                        new Set(multiSelectedQuickReplies).add(
                          quickReply.payload
                        )
                      );
                    }
                : () => onSelect(quickReply.payload)
            }
            selected={
              multiSelect && multiSelectedQuickReplies.has(quickReply.payload)
            }
          />
        ))}
      </Box>
      {multiSelect && (
        <Chip
          // variant="outlined"
          color="primary"
          label={'Confirm'}
          onClick={() =>
            onSelect(
              Array.from(multiSelectedQuickReplies)
                .map(selectedQuickReply => selectedQuickReply)
                .join()
            )
          }
          clickable
          className={classes.confirmButton}
        />
      )}
    </>
  );
};

export default QuickReplies;
