import { all, fork } from 'redux-saga/effects';
import watchOpenConnectionSagas from './chat/sagas';

const rootSaga = function*() {
  yield all([fork(watchOpenConnectionSagas)]);
};

export default rootSaga;
