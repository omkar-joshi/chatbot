import { Box, Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React from 'react';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    height: 'calc(100% - 96px)',
  },
}));

const AppBody: React.FC = ({ children }) => {
  const classes = useStyles();

  return <Box className={classes.root}>{children}</Box>;
};

export default AppBody;
