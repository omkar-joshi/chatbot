import { TimePicker } from '@material-ui/pickers';
import React from 'react';
import { FieldRenderProps } from 'react-final-form';

const TimePickerWrapper: React.FC<FieldRenderProps<string, any>> = props => {
  const {
    input: { name, onChange, value, ...restInput },
    meta,
    ...rest
  } = props;
  const showError =
    ((meta.submitError && !meta.dirtySinceLastSubmit) || meta.error) &&
    meta.touched;

  return (
    <TimePicker
      {...rest}
      name={name}
      helperText={showError ? meta.error || meta.submitError : undefined}
      error={showError}
      inputProps={restInput}
      onChange={onChange}
      value={value === '' ? null : value}
    />
  );
};

export default TimePickerWrapper;
