import { Box, Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React from 'react';
import AvatarVelo from '../../components/AvatarVelo';
import MessageText from '../MessageText';
import MessageTimestamp from '../MessageTimestamp';

const useStyles = makeStyles((theme: Theme) => ({
  botAvatar: {
    marginRight: 22,
  },
}));

interface IBotMessage {
  message: string;
  timestamp: string;
}
const BotMessage: React.FC<IBotMessage> = ({ message, timestamp }) => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Box display="flex" alignItems="flex-end" mb="20px">
        <AvatarVelo size={46} className={classes.botAvatar} />
        <Box>
          <MessageTimestamp type="bot">{timestamp}</MessageTimestamp>
          <MessageText type="bot">{message}</MessageText>
        </Box>
      </Box>
    </React.Fragment>
  );
};

export default BotMessage;
