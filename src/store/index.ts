import reduxWebsocket from '@giantmachines/redux-websocket';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { loadState, saveState } from './localStorage';
import rootReducer from './rootReducer';
import rootSaga from './rootSaga';

const configureStore = () => {
  const reduxWebsocketMiddleware = reduxWebsocket();
  const sagaMiddleware = createSagaMiddleware();
  const persistedState = loadState();
  const store = createStore(
    rootReducer,
    persistedState,
    composeWithDevTools(
      applyMiddleware(reduxWebsocketMiddleware, sagaMiddleware)
    )
  );
  store.subscribe(() => {
    saveState(store.getState());
  });
  sagaMiddleware.run(rootSaga);

  return store;
};

export default configureStore;
