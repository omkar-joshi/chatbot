import { connect } from '@giantmachines/redux-websocket';
import { send } from '@giantmachines/redux-websocket';
import { useDispatch, useSelector } from 'react-redux';
import { IChatState } from '../../store/chat/reducers';
import { IRootReducer } from '../../store/rootReducer';

const useChat = () => {
  const chat = useSelector<IRootReducer, IChatState>(state => state.chat);

  const dispatch = useDispatch();

  const connectRequest = () => {
    dispatch(connect(process.env.REACT_APP_DEFAULT as string));
  };

  const disconnectRequest = () => {
    dispatch(connect(process.env.REACT_APP_DEFAULT as string));
  };

  const sendRequest = (text: string) => {
    dispatch(
      send({
        type: 'message',
        text,
        user: 'Rahul',
        channel: 'socket',
      })
    );
  };

  return {
    chat,
    connectRequest,
    disconnectRequest,
    sendRequest,
  };
};

export default useChat;
