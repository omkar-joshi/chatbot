import {
  AppBar,
  Box,
  IconButton,
  makeStyles,
  Toolbar,
  Typography,
} from '@material-ui/core';
import { Close, KeyboardArrowDown, MoreHoriz } from '@material-ui/icons';
import clsx from 'clsx';
import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { ReactComponent as VelotioLogo } from '../../assets/VelotioLogo.svg';
import AppBody from '../../components/AppBody';
import AppFooter from '../../components/AppFooter';
import AppWindowComponent from '../../components/AppWindow';
import AvatarVelo from '../../components/AvatarVelo';
import fonts from '../../constants/fonts';
import uiSlice from '../../store/ui/slice';
import ChatScreen from '../ChatScreen';
import LandingPage from '../LandingPage';
import MenuOptions from '../MenuOptions';

const useStyles = makeStyles(theme => ({
  iconButton: {
    padding: 'unset',
    color: theme.palette.primary.contrastText,
  },
  logo: {
    '&:hover': {
      backgroundColor: 'unset',
    },
  },

  botName: {
    fontFamily: fonts.RiposteRegular,
    fontSize: 26,
    marginLeft: 10,
  },
  moreIcon: {
    marginLeft: 10,
    marginRight: 10,
  },
  closeIcon: {
    marginLeft: 10,
  },
  arrowDownIcon: {
    marginLeft: 10,
    marginRight: 10,
  },
}));

interface IAppWindow {
  onHide: () => void;
}

const AppWindow: React.FC<IAppWindow> = props => {
  const classes = useStyles();
  const isChatStarted = useSelector(uiSlice.selectors.getUi).isChatStarted;
  const isMenuOpen = useSelector(uiSlice.selectors.getUi).isMenuOpen;
  const dispatch = useDispatch();
  const showMenu = useCallback(
    () => dispatch(uiSlice.actions.showMenu(undefined)),
    [dispatch]
  );
  const closeMenu = useCallback(
    () => dispatch(uiSlice.actions.closeMenu(undefined)),
    [dispatch]
  );

  return (
    <AppWindowComponent>
      <AppBar position="sticky" elevation={0}>
        <Box justifyContent="space-between" px="20px" clone>
          <Toolbar>
            {isChatStarted ? (
              <React.Fragment>
                <Box fontSize="26px" display="flex" alignItems="center">
                  <IconButton
                    className={clsx(classes.iconButton, classes.logo)}
                    disableFocusRipple
                    disableRipple
                  >
                    <AvatarVelo size={46} />
                  </IconButton>
                  <Typography component="span" className={classes.botName}>
                    Velo
                  </Typography>
                </Box>
                <Box fontSize="26px">
                  <IconButton
                    aria-label="Menu"
                    onClick={showMenu}
                    className={clsx(classes.iconButton, classes.moreIcon)}
                  >
                    <MoreHoriz />
                  </IconButton>
                  <IconButton
                    aria-label="Minimize"
                    onClick={props.onHide}
                    className={clsx(classes.iconButton, classes.arrowDownIcon)}
                  >
                    <KeyboardArrowDown />
                  </IconButton>
                  <IconButton
                    aria-label="Exit"
                    // onClick={props.onHide}
                    className={clsx(classes.iconButton, classes.closeIcon)}
                  >
                    <Close />
                  </IconButton>
                </Box>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <IconButton
                  className={clsx(classes.iconButton, classes.logo)}
                  disableFocusRipple
                  disableRipple
                >
                  <VelotioLogo />
                </IconButton>
                <IconButton
                  aria-label="Minimize"
                  onClick={props.onHide}
                  className={classes.iconButton}
                >
                  <KeyboardArrowDown />
                </IconButton>
              </React.Fragment>
            )}
          </Toolbar>
        </Box>
      </AppBar>
      <AppBody>{isChatStarted ? <ChatScreen /> : <LandingPage />}</AppBody>
      <AppFooter />
      <MenuOptions open={isMenuOpen} onClose={closeMenu} />
    </AppWindowComponent>
  );
};

export default AppWindow;
