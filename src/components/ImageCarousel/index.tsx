import { Fab, Theme } from '@material-ui/core';
import { KeyboardArrowLeft, KeyboardArrowRight } from '@material-ui/icons';
import { makeStyles } from '@material-ui/styles';
import React, { useRef } from 'react';
import {
  IBotMessageCarouselTypeData,
  IBotMessageM,
  IListButton
} from '../../types/messages';
import ImageDesc from '../ImageDesc';

const useStyles = makeStyles((theme: Theme) => ({
  leftScroll: {
    marginRight: -15,
    flexShrink: 0
  },
  rightScroll: {
    marginLeft: -15,
    flexShrink: 0
  },
  root: {
    display: 'flex',
    alignItems: 'center',
    overflow: 'hidden'
  },
  container: {
    whiteSpace: 'nowrap',
    overflowX: 'hidden',
    display: 'flex'
  }
}));

interface IImageCarousel {
  data: IBotMessageM;
  onSelect: (item: IListButton) => void;
  disabled: boolean;
}

const ImageCarousel: React.FC<IImageCarousel> = ({
  data,
  onSelect,
  disabled,
}) => {
  const classes = useStyles();
  const carouselContainer = useRef<HTMLDivElement>(null);

  const scrollRight = () => {
    if (carouselContainer && carouselContainer.current) {
      carouselContainer.current.scrollBy({ behavior: 'smooth', left: 280 });
    }
  };
  const scrollLeft = () => {
    if (carouselContainer && carouselContainer.current) {
      carouselContainer.current.scrollBy({ behavior: 'smooth', left: -280 });
    }
  };

  return (
    <div className={classes.root}>
      <Fab
        size="small"
        aria-label="scroll left"
        color="primary"
        className={classes.leftScroll}
        onClick={scrollLeft}>
        <KeyboardArrowLeft />
      </Fab>
      <div ref={carouselContainer} className={classes.container}>
        {(data.data as IBotMessageCarouselTypeData[]).map(
          (item: IBotMessageCarouselTypeData, index: number) => (
            <ImageDesc
              disabled={disabled}
              key={index}
              data={item}
              onSelect={onSelect}
            />
          )
        )}
      </div>
      <Fab
        size="small"
        aria-label="scroll left"
        color="primary"
        className={classes.rightScroll}
        onClick={scrollRight}>
        <KeyboardArrowRight />
      </Fab>
    </div>
  );
};

export default ImageCarousel;
