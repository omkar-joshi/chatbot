import { Box, Link } from '@material-ui/core';
import React from 'react';
import PoweredBy from '../PoweredBy';

const AppFooter: React.FC = () => {
  return (
    <Box
      height={32}
      pr="15px"
      display="flex"
      justifyContent="flex-end"
      alignItems="center"
    >
      <PoweredBy>
        Powered by <Link href="javascript:;">Trained at Velotio</Link>
      </PoweredBy>
    </Box>
  );
};

export default AppFooter;
