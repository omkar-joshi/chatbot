import { createSelector } from 'redux-starter-kit';

export const getChatLength = createSelector(
  ['chat.chat'],
  chat => chat.length
);
