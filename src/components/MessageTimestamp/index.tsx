import { Theme, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React from 'react';
import colorPalette from '../../constants/colorPalette';
import fonts from '../../constants/fonts';

const useStyles = makeStyles<Theme, { type: string }>(theme => ({
  timestamp: {
    textAlign: props => (props.type === 'user' ? 'right' : 'left'),
    fontFamily: fonts.RiposteLight,
    fontSize: 13,
    color: colorPalette.NIGHT_RIDER_GREY,
    marginBottom: 4,
  },
}));

interface IMessageTimestamp {
  type: 'bot' | 'user';
  children: string;
}
const MessageTimestamp: React.FC<IMessageTimestamp> = ({ type, children }) => {
  const classes = useStyles({ type });

  return <Typography className={classes.timestamp}>{children}</Typography>;
};

export default MessageTimestamp;
