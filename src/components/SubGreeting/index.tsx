import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React from 'react';

const useStyles = makeStyles(theme => ({
  subGreeting: {
    fontFamily: 'RiposteLight',
    fontSize: 16,
    color: 'white',
  },
}));

const SubGreeting: React.FC = () => {
  const classes = useStyles();

  return (
    <Typography className={classes.subGreeting}>
      Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
      eirmod tempor.
    </Typography>
  );
};

export default SubGreeting;
