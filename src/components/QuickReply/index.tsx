import { Chip, makeStyles, Theme } from '@material-ui/core';
import React from 'react';
import fonts from '../../constants/fonts';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    marginBottom: 15,
    marginRight: 15,
    height: 40,
    borderRadius: 20,
    fontFamily: fonts.RiposteLight,
    fontSize: 15,
  },
}));

interface IQuickReply {
  quickReplyText: string;
  onClick?: () => void;
  selected?: boolean;
}
const QuickReply: React.FC<IQuickReply> = ({
  quickReplyText,
  onClick,
  selected,
}) => {
  const classes = useStyles();

  return (
    <Chip
      variant={selected ? undefined : 'outlined'}
      color="primary"
      label={quickReplyText}
      onClick={onClick}
      clickable
      className={classes.root}
    />
  );
};

export default QuickReply;
