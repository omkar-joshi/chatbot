import {
  Button,
  Divider,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Theme
} from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React, { useState } from 'react';
import colorPalette from '../../constants/colorPalette';
import fonts from '../../constants/fonts';
import { IBotMessageListTypeData, IBotMessageM } from '../../types/messages';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    border: `1px solid ${colorPalette.BIG_STONE_BLUE_20}`,
    borderRadius: 6,
    marginLeft: 68,
    marginBottom: 20
  },
  primaryText: {
    fontFamily: fonts.RiposteMedium,
    fontSize: 16
  },
  secondaryText: {
    fontFamily: fonts.RiposteRegular,
    fontSize: 14
  },
  button: {
    border: `1.4px solid ${colorPalette.DODGER_BLUE}`,
    fontFamily: fonts.RiposteLight,
    fontSize: 15,
    borderRadius: 8,
    textTransform: 'unset',
    height: 34,
    lineHeight: 'unset',
    width: 74
  },
  divider: {
    margin: `0 ${10}px`
  },
  listItem: {
    paddingTop: 0,
    paddingBottom: 0,
    paddingLeft: 10
  }
}));

interface ISelectList {
  data: IBotMessageM;
  onSelect: (selection: IBotMessageListTypeData) => void;
  isDisabled?: boolean;
}

const SelectList: React.FC<ISelectList> = ({ data, onSelect, isDisabled }) => {
  const classes = useStyles();
  const [selectedItemIndex, setSelectedItemIndex] = useState(-1);

  return (
    <List className={classes.root}>
      {(data.data as IBotMessageListTypeData[]).map((item, index) => (
        <React.Fragment key={index}>
          <ListItem className={classes.listItem}>
            <ListItemText
              primary={item.label}
              secondary={item.description}
              primaryTypographyProps={{ className: classes.primaryText }}
              secondaryTypographyProps={{ className: classes.secondaryText }}
            />
            <ListItemSecondaryAction>
              <Button
                variant={index === selectedItemIndex ? 'contained' : 'outlined'}
                color="primary"
                className={classes.button}
                disabled={isDisabled}
                onClick={
                  selectedItemIndex
                    ? () => {
                        setSelectedItemIndex(index);
                        onSelect(item);
                      }
                    : undefined
                }
                disableRipple>
                Select
              </Button>
            </ListItemSecondaryAction>
          </ListItem>
          {index !== (data.data as IBotMessageListTypeData[]).length - 1 && (
            <Divider className={classes.divider} />
          )}
        </React.Fragment>
      ))}
    </List>
  );
};

export default SelectList;
