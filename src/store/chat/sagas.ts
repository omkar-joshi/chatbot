import { send } from '@giantmachines/redux-websocket';
import { all, put, select, takeLatest } from 'redux-saga/effects';
import { getChatLength } from './selectors';

//-----------  Sagas  -----------//

const tempObject = {
  type: 'message_received',
  text: 'hi - United States - English',
  user: 'Rahul',
  channel: 'socket',
};

export function* openConnectionSaga() {
  try {
    const chatLength = yield select(getChatLength);
    console.log(chatLength);
    if (!chatLength) {
      yield put(send(tempObject));
    }
  } catch (error) {
    console.log(error);
  }
}

//-----------  Watchers  -----------//

export default function* watchOpenConnectionSagas() {
  yield all([takeLatest('REDUX_WEBSOCKET::OPEN', openConnectionSaga)]);
}
