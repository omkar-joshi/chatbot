const fonts = {
  RiposteExtraBold: 'RiposteExtraBold',
  RiposteHeavy: 'RiposteHeavy',
  RiposteLight: 'RiposteLight',
  RiposteMedium: 'RiposteMedium',
  RiposteRegular: 'RiposteRegular',
  RiposteSemiBold: 'RiposteSemiBold',
  RiposteThin: 'RiposteThin',
  RiposteBlack: 'RiposteBlack',
  RiposteBold: 'RiposteBold',
  RiposteExtraLight: 'RiposteExtraLight',
};

export default fonts;
