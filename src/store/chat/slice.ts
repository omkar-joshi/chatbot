// import { Action, createSlice, PayloadAction } from 'redux-starter-kit';

export interface IConnectPayload {
  url: string;
  onOpen: (event: any) => void;
  onError: (event: any) => void;
}

/* const chatSlice = createSlice<IChatState, any>({
  initialState: {
    isConnecting: false,
    chat: [],
    isConnectionOpen: false,
    isSending: false,
    isDisconnecting: false,
  },
  reducers: {
    [WEBSOCKET_CONNECT]: (
      state: IChatState,
      action: PayloadAction<IConnectPayload, 'WEBSOCKET_CONNECT'>
    ) => {
      state.isConnecting = true;
    },
    [WEBSOCKET_SEND]: (
      state: IChatState,
      action: PayloadAction<string, 'WEBSOCKET_SEND'>
    ) => {
      state.isSending = true;
    },
    [WEBSOCKET_DISCONNECT]: (
      state: IChatState,
      action: Action<'WEBSOCKET_DISCONNECT'>
    ) => {
      state.isDisconnecting = true;
    },
  },
  extraReducers: {
    [WEBSOCKET_OPEN]: (state: IChatState, action: Action<'WEBSOCKET_OPEN'>) => {
      state.isConnectionOpen = true;
    },
    [WEBSOCKET_CLOSED]: (
      state: IChatState,
      action: Action<'WEBSOCKET_CLOSED'>
    ) => {
      state.isConnectionOpen = false;
    },
    [WEBSOCKET_MESSAGE]: (
      state: IChatState,
      action: PayloadAction<any, 'WEBSOCKET_MESSAGE'>
    ) => {
      state.chat.push(action.payload);
    },
  },
});

export default chatSlice;
 */
