import { createReducer } from 'redux-starter-kit';
import { IBotMessage, IUserMessage } from '../../types/messages';

export interface IChatState {
  isConnecting: boolean;
  isConnectionOpen: boolean;
  isDisconnecting: boolean;
  isSending: boolean;
  chat: Array<IBotMessage | IUserMessage>;
}

const initialState: IChatState = {
  isConnecting: false,
  chat: [],
  isConnectionOpen: false,
  isSending: false,
  isDisconnecting: false,
};

const chatReducer = createReducer(initialState, {
  'REDUX_WEBSOCKET::OPEN': state => {
    state.isConnectionOpen = true;
  },
  'REDUX_WEBSOCKET::CLOSED': state => {
    state.isConnectionOpen = false;
  },
  'REDUX_WEBSOCKET::MESSAGE': (state, action) => {
    state.chat.push({
      from: 'bot',
      message: JSON.parse(action.payload.message),
    });
    console.log(JSON.stringify(state));
  },
  'REDUX_WEBSOCKET::SEND': (state, action) => {
    console.log(action);
    state.chat.push({ from: 'user', message: action.payload });
  },
});

export default chatReducer;
