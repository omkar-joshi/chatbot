import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React from 'react';

const useStyles = makeStyles(theme => ({
  terms: {
    marginTop: 10,
    fontFamily: 'RiposteLight',
    fontSize: 15,
  },
}));

const ToSOneLiner: React.FC = ({children}) => {
  const classes = useStyles();

  return (
    <Typography align="center" className={classes.terms}>
      {children}
    </Typography>
  );
};

export default ToSOneLiner;
