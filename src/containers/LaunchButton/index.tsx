import { Box, Fab } from '@material-ui/core';
import { Chat } from '@material-ui/icons';
import { makeStyles } from '@material-ui/styles';
import React from 'react';

const useStyles = makeStyles(theme => ({
  launchIcon: {},
}));

interface ILaunchButtonProps {
  onClick: () => void;
}

const LaunchButton: React.FC<ILaunchButtonProps> = props => {
  const classes = useStyles();

  return (
    <Box
      className={classes.launchIcon}
      bgcolor="primary.main"
      position="fixed"
      right={20}
      bottom={20}
      clone
    >
      <Fab
        color="primary"
        aria-label="Launch"
        className={classes.launchIcon}
        onClick={props.onClick}
      >
        <Chat />
      </Fab>
    </Box>
  );
};

export default LaunchButton;
