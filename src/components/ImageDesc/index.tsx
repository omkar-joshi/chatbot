import { Box, Button, makeStyles, Theme, Typography } from '@material-ui/core';
import React from 'react';
import colorPalette from '../../constants/colorPalette';
import fonts from '../../constants/fonts';
import { IBotMessageCarouselTypeData, IListButton } from '../../types/messages';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    display: 'inline-block',
    width: 270,
    // height: 390,
    border: `1px solid ${colorPalette.BIG_STONE_BLUE_20}`,
    borderRadius: 8,
    marginRight: 20,
    marginBottom: 20,
    backgroundClip: 'padding-box',
    whiteSpace: 'normal',
    flexShrink: 0,
    overflow: 'hidden',
    '&:first-child': {
      marginLeft: 40,
    },
    '&:last-child': {
      marginRight: 40,
    },
  },
  img: {
    width: 268,
    height: 150,
  },
  title: {
    fontFamily: fonts.RiposteBold,
    fontSize: 16,
    marginBottom: 6,
  },
  desc: {
    fontFamily: fonts.RiposteRegular,
    fontSize: 14,
    marginBottom: 13,
  },
  button: {
    fontFamily: fonts.RiposteLight,
    fontSize: 15,
    width: '100%',
    marginBottom: 6,
    textTransform: 'unset',
    borderRadius: 8,
    borderWidth: 1.4,
  },
  body: {
    padding: 15,
  },
}));

interface IImageDesc {
  data: IBotMessageCarouselTypeData;
  onSelect: (item: IListButton) => void;
  disabled: boolean;
}
const ImageDesc: React.FC<IImageDesc> = ({ data, onSelect, disabled }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <img src={data.image} className={classes.img} />
      <Box className={classes.body}>
        <Typography className={classes.title}>{data.label}</Typography>
        <Typography className={classes.desc}>{data.description}</Typography>
        {data.buttons.map(item => (
          <Button
            key={item.payload}
            variant="outlined"
            color="primary"
            className={classes.button}
            onClick={() => onSelect(item)}
            disabled={disabled}
          >
            {item.title}
          </Button>
        ))}
      </Box>
    </Box>
  );
};

export default ImageDesc;
